package com.company.liskovsubstitution;

public class LiskovSubstitution {
    public abstract class Account {
        public int balance;
        protected abstract void deposit(int amount);
    }
    public class WithdrawableAccount extends Account{
        @Override
        protected void deposit(int amount) {
            balance += amount;
        }
        protected void withdraw(int amount){
            balance -= amount;
        }
    }
    public class FixedDepositAccount extends Account{
        @Override
        protected void deposit(int amount) {
            balance += amount;
        }
    }
    public class SavingsAccount extends WithdrawableAccount{

    }
    public class CurrentAccount extends WithdrawableAccount{

    }
    public class BankingAppWithdrawalService {
        private WithdrawableAccount withdrawableAccount;

        public BankingAppWithdrawalService(WithdrawableAccount withdrawableAccount) {
            this.withdrawableAccount = withdrawableAccount;
        }

        public void withdraw(int amount) {
            withdrawableAccount.withdraw(amount);
        }
    }

    public void main(){
        WithdrawableAccount wAccount = new WithdrawableAccount();
        FixedDepositAccount fdAccount = new FixedDepositAccount();
        SavingsAccount savingsAccount = new SavingsAccount();
        CurrentAccount currentAccount = new CurrentAccount();

        wAccount.deposit(8000);
        fdAccount.deposit(1000);
        savingsAccount.deposit(7000);
        currentAccount.deposit(9000);

        BankingAppWithdrawalService wAccService = new BankingAppWithdrawalService(wAccount);
        BankingAppWithdrawalService savingsService = new BankingAppWithdrawalService(savingsAccount);
        BankingAppWithdrawalService currentService = new BankingAppWithdrawalService(currentAccount);

        savingsService.withdraw(700);
        currentService.withdraw(900);
        wAccService.withdraw(800);

    }
}
