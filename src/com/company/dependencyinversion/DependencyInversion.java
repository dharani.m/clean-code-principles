package com.company.dependencyinversion;

public class DependencyInversion {
    public class Request{
        public String url;
        public Connection connection;
        public Request(String URL,Connection connection){
            this.url = URL;
            this.connection = connection;
        }
        public void send(){
            connection.get(url);
        }
    }
    public interface Connection{
        void get(String url);
    }
    public class TCPConnection implements Connection{
        @Override
        public void get(String URL){}
    }
    public class UDPConnection implements Connection{

        @Override
        public void get(String url) {}
    }

    public void main(){
        TCPConnection tcp = new TCPConnection();
        Request tcpReq = new Request("www.google.com",tcp);
        tcpReq.send();
        UDPConnection udp = new UDPConnection();
        Request udpReq = new Request("google.com", udp);
    }
}
