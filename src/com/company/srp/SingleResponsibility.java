package com.company.srp;

public class SingleResponsibility {
    public class Customer{
        private String name;
        private String email;
        public Customer(String name,String email){
            this.name = name;
            this.email = email;
        }
    }
    public class PrintCustomer{
        private Customer customer;
        public PrintCustomer(Customer customer){
            this.customer = customer;
        }
        public void welcome(){
            System.out.println("Welcome "+customer.name);
        }
        public void details(){
            System.out.println("Name : "+customer.name+"\nEmail : "+customer.email);
        }
        public void thankyou(){
            System.out.println("Thank you "+customer.name);
        }
    }
    public class ExportCustomer{
        private Customer customer;
        public ExportCustomer(Customer customer){
            this.customer = customer;
        }
        public String toString(){
            return "Name : "+customer.name+", "+"Email : "+customer.email;
        }
        public String[] toStringArray(){
            return new String[]{customer.name,customer.email};
        }
        public String toCSV(){
            return customer.name+","+customer.email;
        }
    }
}
