package com.company.interfaceseggregation;

public class InterfaceSeggregation {
    public interface NonRenewableEnergyCar{
        void putFuel();
    }

    public interface RenewableEnergyCar{
        void putCharge();
    }

    public class PetrolCar implements NonRenewableEnergyCar{
        protected String petrolLevel;
        @Override
        public void putFuel() {
            this.petrolLevel = "Full";
        }
    }
    public class DieselCar implements NonRenewableEnergyCar{
        protected String dieselLevel;
        @Override
        public void putFuel() {
            this.dieselLevel = "FULL";
        }
    }
    public class HybridPetrolCar extends PetrolCar implements RenewableEnergyCar{
        private String batteryLevel;
        @Override
        public void putFuel() {
            petrolLevel = "Full";
        }

        @Override
        public void putCharge() {
            this.batteryLevel = "Full";
        }
    }
    public class HybridDieselCar extends DieselCar implements RenewableEnergyCar{
        private String batteryLevel;
        @Override
        public void putFuel() {
            dieselLevel = "Full";
        }

        @Override
        public void putCharge() {
            this.batteryLevel = "Full";
        }
    }
}
