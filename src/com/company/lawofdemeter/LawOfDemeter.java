package com.company.lawofdemeter;

public class LawOfDemeter {
    public class LOD{
        private int num;
        public LOD(int n){
            this.num = n;
        }
        public int getNum() {
            return num;
        }
        public int getSquare(){
           return num*num;
        }
    }
    public class SecondLOD{
        private int num;
        private LOD lod;
        public SecondLOD(int n){
            this.num = n;
        }
        public int getCube(){
           this.lod = new LOD(this.num);
            return this.lod.getSquare()*num;
        }
        //Violation of Law of Demeter
        public LOD getObject(){
            lod = new LOD(this.num);
            return lod;
        }
    }
    public void main(){
        SecondLOD sLOD = new SecondLOD(7);
        System.out.println(sLOD.getCube());

        //Violation of Law of Demeter
        System.out.println(sLOD.getObject().getSquare());
    }
}
