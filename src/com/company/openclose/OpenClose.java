package com.company.openclose;

class OpenClose {
    public class Car{
        private int seats;
        private String model;
        public void drive(){
            System.out.println("Driving");
        }
    }
    public class PetrolCar extends Car{
        private int mileage;

        public PetrolCar(int mileage) {
            this.mileage = mileage;
        }

        public int getMileage() {
            return mileage;
        }
    }
    public class ElectricCar extends Car{
        private int range;
        public ElectricCar(int range){
            this.range = range;
        }

        public void setRange(int range) {
            this.range = range;
        }

        public int getRange(){
            return this.range;
        }
    }
}